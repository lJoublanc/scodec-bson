# scodec-BSON 

A library for serializing [BSON](http://bsonspec.org/), using [scodec](https://github.com/scodec/). This is used for serialization in the ljoublanc/mongosaur> MongoDB driver.

Copyright (c) 2018 Dinosaur Merchant Bank Limited  

Author: Luciano Joublanc @ljoublanc

The `ULong` data type is adapted from [spire](https://github.com/non/spire),  Copyright (c) 2011-2012 Erik Osheim, Tom Switzer.

## Quickstart

Include the package in your SBT dependencies:

    resolvers += Resolver.bintrayRepo("dmbl","dinogroup")
    libraryDependencies += "com.dinogroup" %% "scodec-bson" % "0.2.3"

The simplest way to use this is with the implicit summoner:

    scala> import scodec.Codec
    import scodec.Codec

    scala> import scodec.protocols.bson.implicits._
    import scodec.protocols.bson.implicits._

    scala> case class Foo(bar : Int, baz : Long)
    defined class Foo

    scala> Codec[Foo].encode(Foo(-1,-2L))
    res10: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(216 bits, 0x1b0000001062617200ffffffff1262617a00feffffffffffffff00))
    //                                                                              size   |I|b a r  |   -1  |L|b a z  |       -2      |00 
    
## Features

Refer to the [BSON Spec](http://bsonspec.org/spec.html) for details. These are the non-terminals that are supported:

| codec/ename  | supported | notes                                          |
| ------------ | --------- | ---------------------------------------------- |
| double       | ✓         |                                                |
| utf8         | ✓         |                                                |
| document     | ✓         |                                                |
| array        | ✓         | Three `array` combinators . See §Arrays.       |
| binary       | -         | Only generic sub-type 0x00. As `ByteVector`.   |
| objectId     | ✓         |                                                |
| boolean      | ✓         |                                                |
| utc (datetime)| ✓        | As `java.time.Instant`                         |
| \`null\`     | ✓         | As `None.type`. Use `option` combinator instead. |
| regex        | ✓         | Needs testing.                                 |
| javascript   | ✗         | Perhaps using Scala.js?                        |
| jsWithScope  | ✗         | Perhaps using Scala.js?                        |
| int32        | ✓         | As `Int`.                                      |
| uint64       | ✓         | AKA Timestamp. `AnyVal` based [spire](https://github.com/non/spire) `ULong` for JVM. |
| int64        | ✓         | As `Long`.                                     |
| decimal128   | ✓         | As `BigDecimal` for JVM. `NaN`/`±∞` not supported.|
| minKey       | ✓         | As `NonTerminal.MinKey`.                       |
| maxKey       | ✓         | As `NonTerminal.MaxKey`.                       |

Note that the spec makes no mention of document field ordering. Using the implicit builder codec in the _Quickstart_ section expects _strictly ordered_ elements, and will fail to decode when these are out-of-order. This is a performance trade-off.

## Advanced Usage

### Elements and Codecs

BSON elements are represented by `EName`s, and become codecs when supplied with
a singleton key type. Similar to the examples in the `scodec` readme, they are easy to use with the `~` combinator:

    scala> import shapeless._
    import shapeless.syntax.singleton._
    import scodec.Codec
    import scodec.protocols.bson._

    scala> type Name = Witness.`'name`.T
    defined type alias Name
    
    scala> type Age = Witness.`'age`.T
    defined type alias Age

    scala>  bson(utf8[Name] ~ int32[Age]) encode ("Molly", 5)
    res1: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(240 bits, 0x1e000000026e616d6500060000004d6f6c6c790010616765000500000000))

Note that unlike the basic `scodec` primitives, you need to supply them with a field name, in square brackets. You also **must** wrap them in the top-level `bson` combinator to have a valid BSON document.

A note on syntax: scala 2.13 introduces singleton types, which allow a much more concise syntax: e.g. `utf8['name]`. Another option is to use shapeless' `narrow` macro.

Decoding will fail if the provided name does not match the encoded name (currently not the case - see #9).

Note there are also implicits to support the `.as[A]` syntax in the root package.For this to work, ensure that your labels are `scala.Symbol`s, [not strings](https://stackoverflow.com/questions/50526509/using-string-instead-of-symbol-in-labelledgeneric-etc).

### Arrays

The implementation of arrays is necessarily convoluted because we try to find a good trade-off between type safety and the dynamic nature of BSON arrays (n.b. BSON arrays [can be heterogenous](http://bsonspec.org/faq.html), and vary in size).

There are three flavours of BSON array combinators. Which one you use depends on whether you know the types (and by implication the size) of the array you are working on. This in turn will typically depend on whether you are encoding or decoding the array.

When encoding, you will typically know the types, so pass an `HList` of `ENames` (note the `HNil` at the end):

    scala> bson(array['numbers](int32 :: double :: utf8 :: HNil)) encode (1 :: 2.0 :: "three" :: HNil)
    heteroBits: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(400 bits, 0x32000000046e756d626572730024000000103000010000000131000000000000000040023200060000007468726565000000))

When decoding, if you know the array to be homogeneous, the basic combinator is the simplest:

    scala> val bits = bson(array['homogeneous](int32)) encode List(-1,-2)
    bits: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(296 bits, 0x2500000004686f6d6f67656e656f75730013000000103000ffffffff103100feffffff0000))

    homogeneous> bson(array['homogeneous](int32)) decodeValue bits.require
    res7: scodec.Attempt[List[Int]] = Successful(List(-1, -2))

> #### TODO :update this once re-implemented in 0.2.0
>
> When you don't know the types of the array you are decoding, use the coproduct `array` combinator. Following on from the _first_ example, imagine you are reading form a MongoDB database and you know what types may appear in the array, but not in which order or how many. Then pass a `CoproductCodecBuilder` (created using the standard scodec `:+:` combinator):
>
>    scala> bson(array(int32 :+: double :+: utf8)) decodeValue heteroBits.require
>    res8: scodec.Attempt[(scodec.protocols.bson.NonTerminals.EName, List[this.Out])] = Successful((numbers,List(Inl(1), Inr(Inl(2.0)), Inr(Inr(Inl(three))))))

### Optional Fields

In the BSON spec there is a `null` type which can be used to represent lack of a value. That type is represented as a `None.type` in scala (n.b. _not_ an `Option[_]`). This _can_ be used, but prefer the `option` combinator instead, which allows working with `scala.Option`. This is also supported in implicit derivation.

### Extended precision types

Hotspot JVM does not have native (primitive) data types for two of BSON's data types, uint64 and decimal128.
To allow future portability across platforms, these have been implemented as _opaque types_ in `BasicTypes`, with operations provided by a number of standard scala type-classes: `Integral` and `Fractional` respectively, as well as `Ordering`.
To use these types, you will require some imports:
```
scala>
import scodec.Codec
import scodec.protocols.bson.syntax._                 // provides arithmetic and comparison infix operators
import scodec.protocols.bson.types.{ULong,Decimal128} // provide type-class instances needed by syntax. NOTE: Avoid wildcard imports due to name clashes with the element codecs.
import scodec.protocols.bson.implicits._              // automatically derive codecs for case-classes

scala> case class Big(decimal: Decimal128, integer: ULong)
defined class Big

scala> import scodec.Codec
import scodec.Codec

scala> Codec[Big] encode Big(9.9999999E-1, 0)
<console>:22: error: type mismatch;
 found   : Double(0.99999999)
 required: scodec.protocols.bson.types.Decimal128
       Codec[Big] encode Big(9.9999999E-1, 0)
                             ^
<console>:22: error: type mismatch;
 found   : Int(0)
 required: scodec.protocols.bson.types.ULong
       Codec[Big] encode Big(9.9999999E-1, 0)
                                           ^
                                           
scala> val r = Decimal128("999999999999999999999999E-1000")
r: scodec.protocols.bson.types.Decimal128 = 9.99999999999999999999999E-977

scala> val i = ULong.fromInt(0)
i: scodec.protocols.bson.types.ULong = 0

scala> Codec[Big] encode Big(r, i)
res6: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(376 bits, 0x2f00000013646563696d616c00210e000000003fcff3fcff3fcff3fcff11696e746567657200000000000000000000))

scala> r + 1
<console>:21: error: value + is not a member of scodec.protocols.bson.types.Decimal128
       r + 1
         ^

scala> r + Decimal128.one
res8: scodec.protocols.bson.types.Decimal128 = 1.00000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000...
```

Note in the above examples, the only way to create these extended precision types are their constructors e.g. `Decimal128(s: String)` or `ULong.fromInt(i: Int)`.
Other numeric types are _not_ automatically cast to these types, as you can see by the failing call to `encode`.
Likewise, to operate on these types you must have imported the infix operators in `syntax._`, which are invariant.
i.e. you can only add a `Decimal128` with another `Decimal128`; numeric types are not upcast automatically.
This seems cumbersome but allows portability across future platforms.

If you wish to override the implementation of either of these extended precision types with your own, you can do so by implementing the `Bson` trait.
See [package.scala](src/main/scala/scodec/protocols/bson/package.scala) for an example of how to do this.
The codecs will be automatically recompiled to use your new types.

## Documentation

If you followed the steps in the above quickstart section, you should find the scaladoc under `scodec-bson/target/scala-2.12/api`. Hosted scaladocs forthcoming.

