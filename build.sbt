organization := "com.dinogroup"

name := "scodec-bson"

scalaVersion := "2.12.7"

// This sets the version. To override locally, create a version.sbt file.
enablePlugins(GitVersioning)

licenses += "MIT" -> url("http://opensource.org/licenses/MIT")

publishMavenStyle := false

bintrayRepository := "dinogroup"

bintrayOrganization := Some("dmbl")

libraryDependencies ++= Seq(
  "org.scodec" %% "scodec-core" % "1.10.3",
  "com.chuusai" %% "shapeless" % "2.3.3",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"
)

scalacOptions ++= Seq("-feature", 
  "-language:higherKinds"
)

initialCommands in consoleQuick := """
  | import shapeless._
  | import shapeless.syntax.singleton._
  |""".stripMargin

initialCommands in console += """
  | import shapeless._
  | import shapeless.syntax.singleton._
  | import scodec.Codec
  """.stripMargin

/* The below is needed for CI on Gitlab/Kubernetes.
 * As this is a library, it is not mean to be run independently,
 * therefore this is a null task.
 */

val stage = taskKey[Unit]("Stage task")

val Stage = config("stage")

stage := { }
