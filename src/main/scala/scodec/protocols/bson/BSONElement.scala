package scodec.protocols.bson

import scodec._
import scodec.bits._
import scodec.codecs._
import shapeless.{Witness,Nat}
import shapeless.ops.nat._

/** The [[BSONElement]] type can be thought of as a partially applied codec.
  * It knows they type [[opcode]] and  how to encode a [[value]], but
  * not label, of an element. A `BSONElement` e.g. `int32` becomes a
  * `BSONCodec` by specifying it's label (in square brackets) e.g.
  * `int32['age]`. 
  * @param opcode A byte used to determine the type of value of this element.
  * @param value A codec for the value part of this element.
  */
class BSONElement[A](val opcode: Byte, val value: Codec[A]) {

  private val opcodeBits = 
    byte encode opcode

  /** Encode without type checking. */
  protected[bson] def encodeUnsafe(labelBits: Attempt[BitVector])(a: A): Attempt[BitVector] = {
      for { b <- opcodeBits
            s <- labelBits
            a <- value encode a }
      yield b ++ s ++ a
  }

  /** Decode without type checking. */
  protected[bson] def decodeUnsafe(label: String)(bits: BitVector): Attempt[DecodeResult[A]] = {

    // Need this to be materialized for UnknownDiscriminator.
    lazy val error = apply[String]{ new Witness {
        type T = String
        val value = label
      }
    }
     
    byte decode bits flatMap { 
       case DecodeResult(b,rem) =>
         if (b != opcode)
           Attempt failure error.UnknownDiscriminator(b ~ "???", Nil)
         else 
           cstring decode rem flatMap {
             case DecodeResult(s,rem) =>
               if (s != label)
                 Attempt failure error.UnknownDiscriminator(b ~ s, List(f"0x$b%02x"))
               else 
                 value withContext f"0x$b%02x/'$s%s'" decode rem
         }
     } 
  }

  /** Builder for a BSON element codec.
    * @tparam K The type of the key (label). Currently `String` and `Symbol` supported. Note that for implicit resolution, you must use `Symbol`.
    * TODO: change the discriminator to Byte ~ K, for compile-time checks.
    */
  def apply[K](implicit K: Witness.Aux[K]): BSONCodec[A] =   
    new Codec[A] with KnownDiscriminatorType[Byte ~ String] {

      val label: String = K.value match {
        case s: String => s
        case s: Symbol => s.name.toString
        case x => x.toString
      }

      val labelBits = cstring encode label

      def decode(bits: BitVector): Attempt[DecodeResult[A]] = 
        decodeUnsafe(label)(bits)

      def encode(a: A): Attempt[BitVector] =
        encodeUnsafe(labelBits)(a)

      def sizeBound: SizeBound = 
        byte.sizeBound + SizeBound.atLeast(labelBits.require.size) + value.sizeBound
    }
}
