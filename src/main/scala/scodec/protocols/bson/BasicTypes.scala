package scodec.protocols.bson

import scodec.Codec
import scodec.{codecs => sc}
import BasicTypes._

/** Codecs for the ''value'' part of a BSON Elements. 
  *
  * Each type must be serialized in little-endian format, according to the
  * spec. `ULong` and `Decimal128` are purposely left abstract so these can be
  * overriden for each platform. e.g. scala-native supports `unsigned long`.  
  *
  * </p> You most likely do not want to use these codecs directly. See
  * [[BSONElement]] instead.  These are made public as the MongoDB wire
  * protocol uses the same encodings.
  */
trait BasicTypes {

  /** Type used to represent 64-bit unsigned integer and also BSON [[Timestamp]]. */
  type ULong

  /** Algebra and ordering for `ULong`. */
  implicit def ULong : Integral[ULong] with Ordering[ULong]

  /** BSON Timestamp accessors for `ULong`. */
  implicit def Timestamp : Timestamp[ULong]

  /** Type used to represent extended precision 128-bit floating point. */
  type Decimal128

  /** Algebra and ordering for `Decimal128`.
    * @example {{{
    * import scodec.protocols.bson.types.Decimal128
    * import scodec.protocols.bson.syntax._
    * 
    * Decimal128("1.2345E+99") + Decimal128("0.0001E+99")
    * //res0: scodec.protocols.bson.types.Decimal128 = 1.2346E+99
    * }}}
    */
  implicit def Decimal128: IEEE754Algebra[Decimal128]

  /**1 byte (8-bits)*/
  def byte : Codec[Byte] = sc.byte

  /**4 bytes (32-bit signed integer, two's complement)*/
  def int32 : Codec[Int] = sc.int32L

  /**8 bytes (64-bit signed integer, two's complement)*/
  def int64 : Codec[Long] = sc.int64L

  /**8 bytes (64-bit unsigned integer)*/
  def uint64 : Codec[ULong]

  /**8 bytes (64-bit IEEE 754-2008 binary floating point)*/
  def double : Codec[Double] = sc.doubleL

  /**16 bytes (128-bit IEEE 754-2008 decimal floating point)*/
  def decimal128 : Codec[Decimal128]
}

object BasicTypes {
  trait IEEE754Algebra[T]
  extends Fractional[T]
  with Ordering[T] {
    /** Constructs a `Decimal128` from canonical string represetnation as
      * described in the [[https://github.com/mongodb/specifications/blob/master/source/bson-decimal128/decimal128.rst#id11
      * specification]]
      * @see [[http://speleotrove.com/decimal/daconvs.html]]
      */
    def apply(s: String): T
  }
}
