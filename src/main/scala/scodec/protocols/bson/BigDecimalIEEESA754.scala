package scodec.protocols.bson

import scodec._
import scodec.bits.BitVector

import annotation.tailrec

/** Implements `Decimal128` as a [[http://speleotrove.com/decimal/dbspec.html
  * IEEE-SA 754 Decimal]] represented by `BigDecimal` in the JVM.
  *
  * A limitation is that `BigDecimal` has no representation for infinity or
  * NaN, so this codec can't encode those values either.
  *
  * Below are the coefficients values that are correspond to the constructor
  * arguments.
  * <table border="1" cellspacing="0" cellpadding="4">
  * <tbody><tr valign="top" line="">
  * <td width="48%">Format
  * </td><td><b>decimal32</b>
  * </td><td><b>decimal64</b>
  * </td><td><b>decimal128</b>
  * </td></tr><tr valign="top">
  * <td>Format length
  * </td><td>32
  * </td><td>64
  * </td><td>128
  * </td></tr><tr valign="top">
  * <td>Exponent continuation length (<i>ecbits</i>)
  * </td><td>6
  * </td><td>8
  * </td><td>12
  * </td></tr><tr valign="top">
  * <td>Coefficient continuation length
  * </td><td>20
  * </td><td>50
  * </td><td>110
  * </td></tr><tr valign="top">
  * <td>Total Exponent length
  * </td><td>8
  * </td><td>10
  * </td><td>14
  * </td></tr><tr valign="top">
  * <td>Total Coefficient length in digits
  * </td><td>7
  * </td><td>16
  * </td><td>34
  * </td></tr><tr valign="top">
  * <td>E<sub>limit</sub>
  * </td><td>191
  * </td><td>767
  * </td><td>12287
  * </td></tr><tr valign="top">
  * <td>E<sub>max</sub>
  * </td><td>96
  * </td><td>384
  * </td><td>6144
  * </td></tr><tr valign="top">
  * <td>E<sub>min</sub>
  * </td><td>–95
  * </td><td>–383
  * </td><td>–6143
  * </td></tr><tr valign="top">
  * <td>bias
  * </td><td>101
  * </td><td>398
  * </td><td>6176
  * </td></tr></tbody></table>
  *
  * @param length Corresponds to `Format length` (see table).
  * @param expContLen Corresponds to `Exponent continuation length` (see table).
  * @param coeffContLen Corresponds to `Coefficient continuation length` (see table).
  */
class BigDecimalIEEESA754(
  val length: Int,
  expContLen: Int,
  coeffContLen: Int,
  totExpLen: Int,
  val totCoeffLenDigits: Int, // aka p, precision.
  ) extends Codec[BigDecimal] {
  import BigDecimalIEEESA754._
  import scodec.codecs._
  import Integral.Implicits._

  private val `10` = BigInt(java.math.BigInteger.TEN)

  private val `1000` = BigInt(1000)

  /* These constants are derived from the appendix. */

  /** The largest (positive) exponent after adjusting for `bias`. */
  val ELimit = 3 * scala.math.pow(2,expContLen).toInt - 1

  /** Smallest (most negative) exponent. */
  val EMin = -ELimit / 2

  /** Largest exponent. */
  val EMax = ELimit / 2 + 1

  /** Exponent of smallest subnormal number (i.e. closest to 0). */
  val ETiny = EMin - (totCoeffLenDigits - 1)

  /** This is added to `ETiny` to make it unsigned. */
  val bias = -ETiny

  /** The largest encodable normal (i.e. ≥ 1) number. */
  val maxValue =
    BigDecimal(BigInt(10).pow(totCoeffLenDigits) - BigInt(1), -(EMax-(totCoeffLenDigits-1)))

  /** The smallest positive encodable normal (i.e. ≥ 1) number. 
    * @note This is '''not''' the same as `-maxValue`.
    */
  val minValue = BigDecimal(1, scale = -EMin)

  /** The smallest positive encodable subnormal (i.e. 0 ≤ x ≤ 1) number. */
  val tinyValue = BigDecimal(1, scale = -ETiny)

  /** @see [[http://speleotrove.com/decimal/dbspec.html]] 
    * @todo re-write using mutable state for efficiency.
    */
  def decode(_bits: BitVector): scodec.Attempt[DecodeResult[BigDecimal]] = {
    import scodec.bits._
    for { res <- bool withContext "sign" decode (_bits)
          DecodeResult(isNegative,rem) = res
          res <- bits(5).emap{ bits =>
                   if (bits.take(4)  == bin"1111") //1111x
                     Attempt failure Err("Infinity and NaN not supported")
                   else if (bits.take(2) == bin"11") //11cde => (cd,100e)
                     Attempt successful (bits slice (2,4), bin"100" ++ bits.takeRight(1))
                   else { // abcde => (ab,0cde)
                     val (l,r) = bits.splitAt(2)
                     Attempt successful ((l, bin"0" ++ r))
                   }
                 }.decodeOnly.withContext("combination").decode(rem)
          DecodeResult((expMSB, coeffMSD),rem) = res
          res <- bits(expContLen).decode(rem)
          DecodeResult(expCont, rem) = res
          biasedExp <- uint(totExpLen).withContext("exponent").decodeValue(expMSB ++ expCont)
          res <- bits(coeffContLen).decode(rem)
          DecodeResult(coeffCont, rem) = res
          coeff <- dpd.withContext("coefficient").decodeValue(coeffMSD ++ coeffCont)
    } yield DecodeResult(BigDecimal(if (isNegative) -coeff else coeff, -(biasedExp - bias)),rem)
  }
 
  def encode(i: BigDecimal): scodec.Attempt[BitVector] = {
    import scodec.bits._
     
    val sign: Boolean = i.signum match {
      case -1 => true
      case _ => false
    }

    val coeffAndExp: Attempt[(BigInt,Int)] = {
      import scala.math.Numeric.BigDecimalIsFractional._
      import scala.math.Numeric.BigIntIsIntegral
      val j = i.abs.setScale(i.scale)
      if (j.precision > totCoeffLenDigits)
        Attempt failure OutsideRangeErr(s"The number $i is beyond the representable precision of $totCoeffLenDigits digits.")
      else if (j > one)
        Attempt fromEither Either.cond(
          j.ulp + one >= minValue && j <= maxValue,
          BigInt(j.underlying.unscaledValue) -> (-j.scale + bias),
          OutsideRangeErr(s"The number $i is outside the representable normal range.")
        )
      else if (j == one) 
        Attempt successful (BigIntIsIntegral.one -> bias)
      else if (j > zero)
        Attempt fromEither Either.cond(
          j.ulp >= tinyValue,
          BigInt(j.underlying.unscaledValue) -> (-j.scale + bias), //TODO: 0.99999999..
          OutsideRangeErr(s"The number $i is outside the representable subnormal range.")
        )
      else 
        Attempt successful (BigIntIsIntegral.zero -> 0)
    }

    for { signBit <- bool withContext "sign" encode sign
          coeffAndExpMaybe <- coeffAndExp
          (coeffValue,expValue) = coeffAndExpMaybe
          expBits <- uint(totExpLen) withContext "exponent" encode expValue
          coeffBits <- dpd withContext "coefficient" encode coeffValue map (_ padLeft (coeffContLen + 4)) // +4 is only guaranteed for 32,64,128 bits?
          (expMSB, expCont) = expBits splitAt 2
          (coeffMSD, coeffCont) = coeffBits splitAt 4 
    } yield
      if (coeffMSD(0) == false)
        signBit ++ expMSB ++ (coeffMSD drop 1) ++ expCont ++ coeffCont
      else
        signBit ++ bin"11" ++ expMSB ++ (coeffMSD drop 3) ++ expCont ++ coeffCont
  }
    
  def sizeBound: SizeBound = SizeBound exact length
}

object BigDecimalIEEESA754 {

  /** Emitted when trying to encode a number that is outside the representable 
    * range of the codec. This can occurr when the number is too large in absolute
    * value e.g. ±9E+99999.., but also when it is too small/has too many decimal
    * places e.g. ±7E-999999. Note that this limit is different for normal and
    * subnormal numbers.
    */
  case class OutsideRangeErr(message: String, context: List[String] = Nil) extends Err {
    def pushContext(ctx: String) =  Err.General(message, ctx :: context)
  }

  /** 128-Bit decimal codec conforming to IEEE-SA 754 2008. */
  val decimal128 = 
    new BigDecimalIEEESA754(
      length = 128,
      expContLen = 12,
      coeffContLen = 110,
      totExpLen = 14,
      totCoeffLenDigits = 34)

  /** 64-Bit decimal codec conforming to IEEE-SA 754 2008. */
  val decimal64 = 
    new BigDecimalIEEESA754(
      length = 64,
      expContLen = 8,
      coeffContLen = 50,
      totExpLen = 10,
      totCoeffLenDigits = 16)

  /** 32-Bit decimal codec conforming to IEEE-SA 754 2008. */
  val decimal32 = 
    new BigDecimalIEEESA754(
      length = 32,
      expContLen = 6,
      coeffContLen = 20,
      totExpLen = 8,
      totCoeffLenDigits = 7)

  /** Decodes DPD into BigInt. 
    * @todo [[https://github.com/scodec/scodec/issues/126]]
    */
  lazy val dpd: Codec[BigInt] = {
    import scodec.codecs.{vpbcd,pbcd}
    import scala.math.Numeric.BigIntIsIntegral.zero
    new DenselyPackedDecimal {}
     .emap{ bcd =>
       @tailrec
       def f(bcd: BitVector)(acc: Attempt[BigInt], i: Int): Attempt[BigInt] =
         if (bcd.isEmpty)
           acc
         else
           vpbcd.decodeValue(bcd.takeRight(12)) match { 
             case Attempt.Successful(msd) =>
               f(bcd.dropRight(12))(acc map (BigInt(msd)*BigInt(10).pow(i) + _), i + 3)
             case fail => fail.map(_.toLong)
           }
       f(bcd)(Attempt successful zero, 0)
     }
     .econtramap{ i : BigInt =>
       @tailrec 
       def f(i: BigInt)(acc: Attempt[BitVector]): Attempt[BitVector] =
         i /% 1000 match {
           case (zro,i) if zro == zero =>
             acc flatMap (lsds => vpbcd encode i.toLong map (_ ++ lsds))
           case (div,rem) =>
             vpbcd encode rem.toLong match {
               case Attempt.Successful(msb) =>
                 f(div)(acc map (msb.padLeft(12) ++ _))
               case failure => failure
             }
         }
       f(i)(Attempt successful BitVector.empty) 
     }
     .fuse
  }
}
