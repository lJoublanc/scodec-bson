package scodec.protocols.bson

import scodec.{Codec,Decoder,Attempt}
import scodec.bits.{ByteVector,BitVector}
import scodec.codecs.{ToHListCodec,CoproductCodecBuilder,CoproductBuilderAuto}
import scodec.protocols.bson.NonTerminals._
import shapeless.{::, HNil, Witness, Lazy, HList, Coproduct, LabelledGeneric}
import shapeless.ops.record.Keys
import shapeless.ops.hlist.{Mapper,Mapped,Comapped,Zip,LiftAll,LeftFolder}
import shapeless.labelled.{FieldType,field}

import java.time.Instant

/** Public API for BSON codec.
  *
  * This trait is made public to allow you to implement codecs with your own
  * data types for [[BasicTypes#ULong]] and [[BasicTypes#Decimal128]], which
  * have no native JVM  implementation. For example, you could use
  * [[https://github.com/non/spire spire]] data types for this, instead of the
  * default JVM implementation which currently uses `BigInt` and `BigDecimal`
  * to avoid external dependencies).
  */
trait Bson extends NonTerminals { e => //`e` for elements
  import e.types.{ULong,Decimal128}

  /** Syntax for [[Timestamp]], `ULong`, and `Decimal128` operations. */
  object syntax
  extends Integral.ExtraImplicits
  with Fractional.ExtraImplicits
  with Ordering.ExtraImplicits
  with Timestamp.Syntax

  protected[bson] trait CodecDerivations {
    import scodec.codecs.fail
    import scodec.{Err,Attempt}
    import shapeless._

    /** This overrides `scodec.Codec.deriveRecord` in implicit resolution.
      * It prevents picking up the top-level [[bson]] codec, once it's been
      * resolved as the top element.
      * 
      * @param headCodec A codec decoding a ''named'' element.
      *                  Contrast with `scodec.Codec.deriveRecord` which 
      *                  only requires a `Codec[VH]`.
      */
    implicit def deriveRecord[KH <: Symbol, VH, TRec <: HList](implicit
        headName: Witness.Aux[KH],
        headCodec: Lazy[BSONElement[VH]],
        tailAux: Lazy[BSONCodec[TRec]]
      ) : BSONCodec[FieldType[KH,VH] :: TRec] = 
        scodec.codecs.lazily {
          val headFieldCodec: BSONCodec[FieldType[KH, VH]] = 
            headCodec.value.apply[KH]
              .toField[KH]
              .asInstanceOf[BSONCodec[FieldType[KH,VH]]]
          headFieldCodec :: tailAux.value
        }.asInstanceOf[BSONCodec[FieldType[KH,VH] :: TRec]]
    
    implicit val deriveHNil: BSONCodec[HNil] =
      scodec.codecs.HListCodec.hnilCodec.asInstanceOf[BSONCodec[HNil]]

  }

  /** Implicits required to automatically derive codecs usingthe `codecs.Codec`
    * summoner.
    */
  object implicits extends CodecDerivations {

    /* Top-level i.e. anonymous document. */
    implicit def bson[A, Rec <: HList](
      implicit
      labels : LabelledGeneric.Aux[A,Rec],
      e_list : Lazy[BSONCodec[Rec]]
    ) : Codec[A] =
      e.bson(e_list.value.xmap(labels from _, labels to _))

    implicit def double: BSONElement[Double] = e.double

    implicit def utf8: BSONElement[String] = e.utf8

    /* Embedded document. */
    implicit def document[A, Rec <: HList](
        implicit 
        labels : LabelledGeneric.Aux[A,Rec],
        e_list : Lazy[BSONCodec[Rec]]
      ): BSONElement[A] =
          e.document(e_list.value.xmap(labels from _, labels to _))

    implicit def arrayHeterogeneous[M <: HList, ML <: HList, L <: HList](
        implicit 
        e_list: LiftAll.Aux[BSONElement,L,M],
        comapped: Comapped.Aux[M,BSONElement,L],
        zipWithValue: Zip.Aux[M :: L :: HNil, ML],
        foldLeft: LeftFolder.Aux[ML,(Attempt[BitVector],Int),ZipEncode.type,(Attempt[BitVector],Int)],
        decoder: DecodeHArray.Aux[L,M]
    ): BSONElement[L] = array[M,ML,L](e_list.instances)

    
    implicit def arrayHomogenous[A](implicit element : BSONElement[A]): BSONElement[List[A]] =
      e.array[A](element)

/*

    implicit def arrayDynamic[KV <: Coproduct, V <: Coproduct, VK <: Coproduct, L <: HList](
      implicit
      zipConst : shapeless.ops.coproduct.ZipConst.Aux[String,V,VK],
      swap : shapeless.ops.coproduct.Mapper.Aux[NonTerminals.Swap.type,VK,KV],
      e_list : CoproductBuilderAuto.Aux[KV,KV,L],
      dropIdx : shapeless.ops.coproduct.Mapper.Aux[NonTerminals.DropIdx.type,KV,V]
    ) : BSONCodec[List[V]] = 
        e.array[KV,V,VK,L](Codec.coproduct[KV])
*/

    implicit def binary: BSONElement[ByteVector] = e.binary

    implicit def objectId: BSONElement[ObjectId] = e.objectId

    implicit def boolean: BSONElement[Boolean] = e.boolean

    implicit def utc: BSONElement[Instant] = e.utc

    implicit def int32: BSONElement[Int] = e.int32

    implicit def uint64: BSONElement[ULong] = e.uint64

    implicit def int64: BSONElement[Long] = e.int64

    implicit def decimal128: BSONElement[Decimal128] = e.decimal128

    implicit def option[A](implicit A: BSONElement[A]): BSONElement[Option[A]] =
      e.option[A](A,encodeNulls = false)

  }
}
