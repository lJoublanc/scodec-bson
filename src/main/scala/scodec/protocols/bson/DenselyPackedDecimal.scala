package scodec.protocols.bson

import scodec.{Attempt,Err,Codec,SizeBound,DecodeResult}
import scodec.codecs._
import scodec.bits.BitVector

import scala.math.Integral.Implicits._

/** Codec that encodes [[https://en.wikipedia.org/wiki/Binary-coded_decimal BCD]] 
  * to [[https://en.wikipedia.org/wiki/Densely_packed_decimal DPD]].
  * BCD uses 4 bits to represent a single digit 0-9, whereas DPD uses 10 bits for
  * up to three decimals.
  * This is used to encode the constant part of [[Decimal128]].
  *
  * This returns the decimals in a BCD `BitVector`. You can then use `scodec.codecs.vpbcd`
  * combinator to change this into JVM/binary representation.
  * 
  * Some examples:
  *
  * <table cellpadding="5" cellspacing="0" border="1">
  * <tbody><tr valign="top">
  *   <th>BCD</th>
  *   <th>Densely Packed</th>
  *   </tr>
  * <tr>
  *   <th>005</th>
  *   <td align="center">000 000 0101</td>
  *   </tr>
  * <tr>
  *   <th>009</th>
  *   <td align="center">000 000 1001</td>
  *   </tr>
  * <tr>
  *   <th>055</th>
  *   <td align="center">000 101 0101</td>
  *   </tr>
  * <tr>
  *   <th>079</th>
  *   <td align="center">000 111 1001</td>
  *   </tr>
  * <tr>
  *   <th>099</th>
  *   <td align="center">000 101 1111</td>
  *   </tr>
  * <tr>
  *   <th>555</th>
  *   <td align="center">101 101 0101</td>
  *   </tr>
  * <tr>
  *   <th>999</th>
  *   <td align="center">001 111 1111</td>
  * </tr>
  * 
  * </tbody></table>
  * @see [[http://speleotrove.com/decimal/DPDecimal.html]]
  * @return Three decimals packed in BCD format.
  * @todo optimize: precompute into a table.
  */
trait DenselyPackedDecimal extends Codec[BitVector] {
  import DenselyPackedDecimal._
  import math.min

  /** @param bits Densley Packed Decimals `BitVector`
    * @return Same number of Binary Coded Decimals as the input in a `BitVector`.
    */
  def decode(dpd: BitVector): Attempt[DecodeResult[BitVector]] = {
    val (div,rem) = dpd.length /% 10L
    val (padl,dropr) = rem match {
      case 0 =>
        (div * 10L, 0L)
      case 1 | 2 | 3 =>
        (div * 10L, rem)
      case 4 | 7 =>
        ((div + 1L) * 10L, 0L)
      case 5 | 6 =>
        ((div + 1L) * 10L, rem - 4L)
      case 8 | 9 =>
        ((div + 1L) * 10L, rem - 7L)
      case _ => 
        throw new Error("Impossible")
    }
    val (err,bcd) = 
      Codec.decodeAll[BitVector,BitVector](dpd.dropRight(dropr).padLeft(padl))(BitVector.empty, _ ++ _)(dpd => threeDpds.decodeValue(dpd).require)(bits(10))

    Attempt.fromErrOption(err,bcd).map{ bcd => 
      DecodeResult(
        if (rem >= 7) bcd drop 4 else if (rem >= 4) bcd drop 8 else bcd,
        dpd.takeRight(dropr)
      )
    }
  }

  /** @param bcd: Binary Coded Digits `BitVector`
    * @return Same number of Densley Packed Decimals as the input, in a `BitVector`.
    */
  def encode(bcd: BitVector): Attempt[BitVector] = {
    if (bcd.isEmpty)
      Attempt successful BitVector.empty
    else {
      val (div,rem) = bcd.length /% 12L
      val a = rem match {
        case 0 => Attempt successful BitVector.empty
        case 4 => Attempt successful BitVector.low(8)
        case 8 => Attempt successful BitVector.low(4)
        case _ => Attempt failure Err(s"BCD ${bcd.toBin} is not evenly divisible by 4-bits")
      } 
      a flatMap { padl =>
        (padl ++ bcd).consumeThen(12)(
          e => Attempt failure Err(e),
          (nibbles, rem) => threeDpds encode nibbles flatMap { dpdMsb =>
            encode(rem).map { dpdLsb =>
              dpdMsb.drop {
                padl.length match {
                  case 0 => 0
                  case 4 => 10 - 7
                  case 8 => 10 - 4
                  case _ => throw new Error("Impossible")
                }
              } ++ dpdLsb
            }
          }
        )
      }
    }
  }

  def sizeBound: SizeBound = SizeBound.unknown
}

object DenselyPackedDecimal {
  /** Codec that encodes 3 decimals in BCD (12-bits) into DCD (10-bits) */
  protected val threeDpds: Codec[BitVector] = 
    Codec( bcd =>
      listOfN(provide(12),bool).map {
        case List(a, b, c, d, e, f, g, h, i, j, k, m) =>
          val p = b | (a & j) | (a & f & i)
          val q = c | (a & k) | (a & g & i)
          val r = d
          val s = (f & (!a | !i)) | (!a & e & j) | (e & i)
          val t = g  | (!a & e & k) | (a & i)
          val u = h
          val v = a | e | i
          val w = a | (e & i) | (!e & j)
          val x = e | (a & i) | (!a & k)
          val y = m
          BitVector bits Iterable(p, q, r, s, t, u, v, w, x, y)
      }.decodeValue(bcd),
      dpd =>
      listOfN(provide(10),bool).map{ 
        case List(p, q, r, s, t, u, v, w, x, y) =>
          val a = (v & w) & (!s | t | !x)
          val b = p & (!v | !w | (s & !t & x))
          val c = q & (!v | !w | (s & !t & x))
          val d = r
          val e = v & ((!w & x) | (!t & x) | (s & x))
          val f = (s & (!v | !x)) | (p & !s & t & v & w & x)
          val g = (t & (!v | !x)) | (q & !s & t & w)
          val h = u
          val i = v & ((!w & !x) | (w & x & (s | t)))
          val j = (!v & w) | (s & v & !w & x) | (p & w & (!x | (!s & !t)))
          val k = (!v & x) | (t & !w & x) | (q & v & w & (!x | (!s & !t)))
          val m = y
          BitVector bits Iterable(a, b, c, d, e, f, g, h, i, j, k, m)
      }.decode(dpd)
    )
}
