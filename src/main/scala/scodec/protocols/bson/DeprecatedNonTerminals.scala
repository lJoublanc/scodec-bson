package scodec.protocols.bson

import scodec.Codec
import scodec.codecs.literals.constantIntCodec
import scodec.codecs.{ignore,provide}
import shapeless.Witness
import shapeless.labelled.FieldType

/** This trait houses non-terminal elements which have been deprecated in the
  * official specification.
  */
@deprecated("By offical BSON spec", "1.1")
trait DeprecatedNonTerminals { t : NonTerminals =>
  import DeprecatedNonTerminals._

  def undefined: BSONElement[Undefined.type] = new BSONElement(0x06, provide(Undefined))

  def dbPointer: BSONElement[DBPointer] = new BSONElement(0x07,
    string.widenOpt(DBPointer.apply,DBPointer.unapply) <~ ignore(12)
  )

  def symbol: BSONElement[Symbol] =
    new BSONElement(0x0E, string.widenOpt(Symbol,Symbol.unapply))
}

/* These are value classes to avoid clashes with implicit resolution of codecs. */
@deprecated("By offical BSON spec", "1.1")
object DeprecatedNonTerminals {

  case object Undefined

  case class DBPointer(value : String)

  case class Symbol(value : String)
}
