package scodec.protocols.bson

import scodec._
import scodec.bits.{BitVector,ByteVector}
import scodec.codecs.literals.constantIntCodec
import scodec.codecs.{~,variableSizeBytes,variableSizePrefixedBytes,ignore,cstring,provide,bool,KnownDiscriminatorType}
import shapeless.labelled.{FieldType,field}
import shapeless.{:+:,HList,Coproduct,Witness,LabelledGeneric,::,HNil,Lazy}
import shapeless.ops.hlist.{Zip,Comapped,LeftFolder}

import java.time.Instant
import java.util.regex.Pattern
import scala.util.matching.Regex

/** Non-terminals of the BSON specification.
  * Doc attribution : Most comments have been copied from the offical spec.
  * @see [[http://bsonspec.org/spec.html]]
  */
trait NonTerminals {
  import NonTerminals._

  /** Codecs for the value part of a BSON Element */
  val types : BasicTypes

  /** The top-level document. A valid BSON document must be framed with it's size
    * and followed by a null byte.*/
  def bson[A](e_list: Codec[A]) : Codec[A] =
    variableSizeBytes(types.int32.xmap(_ - 5,_ + 5), e_list) <~ 0x00

  /** 64-bit binary floating point. */
  def double: BSONElement[Double] = new BSONElement[Double](0x01,types.double)

  /** UTF-8 string. */
  def utf8: BSONElement[String] = new BSONElement[String](0x02,string)

  /** Embedded document. For top-level doc, use [[bson]]. This differs from
    * the top-level document in that it is named, whereas the top-level
    * document is anonymous.
    *
    * @note provide type parameter `C` explicitly to avoid ambiguity.
    */
  def document[A, C[a] <: Codec[a]](e_list: C[A]): BSONElement[A] = new BSONElement(0x03, bson[A](e_list))

  /** An embedded, labelled, document.
    * This overload is purely syntax to allow specifying the name/label before 
    * elements.
    * @return A `Codec` (not a `BSONElement`).
    * @example {{{
    * val c = bson(document['address](int32['number] ~ string['street] ~ int32['zip]))
    * c encode (1 ~ "Beverly Hills" ~ 90210)
    * }}}
    */
  def document[K]: DocumentSyntax[K] = new DocumentSyntax[K](this)

  /**
    * The document for an array is a normal BSON document with integer values
    * for the keys, starting with 0 and continuing sequentially. For example,
    * the array ['red', 'blue'] would be encoded as the document {'0': 'red',
    * '1': 'blue'}. The keys must be in ascending numerical order.
    * 
    * This is syntax to return a `BSONCodec` rather than a `BSONElement`. The latter
    * is only useful when using combinators.
    * @example {{{
    * val c = bson(array['scores](int32))
    * c encode List(23,12,20,15)
    * }}}
    */
  def array[K]: ArraySyntax[K] = new ArraySyntax[K](this)

  /** Array (Heterogeneous).
    *
    * Prefer this combinator when
    * <ul>
    *   <li/> Encoding, when both the types and number of elements are known at compile time.
    * </ul>
    * @tparam M `HList` of `BSONElement`.
    * @tparam L `HList` of underlying types aligned with `M`.
    * @param e_list An `HList` of `BSONElement`s.
    */
  def array[M <: HList, ML <: HList, L <: HList](e_list : M)(
      implicit 
      comapped: Comapped.Aux[M,BSONElement,L],
      zipWithValue: Zip.Aux[M :: L :: HNil, ML],
      foldLeft: LeftFolder.Aux[ML,(Attempt[BitVector],Int),ZipEncode.type,(Attempt[BitVector],Int)],
      decoder: DecodeHArray.Aux[L,M]
  ): BSONElement[L] = 
    new BSONElement(0x04, bson {
      new Codec[L] { 
        def encode(m : L): Attempt[BitVector] = 
          (e_list zip m) //TODO create a typeclass to do this, so we save one pass on the HList.
            .foldLeft(Attempt.successful(BitVector.empty) -> 0)(ZipEncode)
            ._1

        def decode(bits: BitVector): Attempt[DecodeResult[L]] =
          decoder(e_list,Attempt successful bits)

        def sizeBound: SizeBound = SizeBound.unknown // may be empty
      }
    })

  /** Array (Homogeneous).
    * Prefer this combinator when
    * <ul>
    *   <li/> Decoding, when the elements are all of the same type, but their number is unknown at run-time.
    * </ul>
    */
  def array[A](element : BSONElement[A]): BSONElement[List[A]] = 
    new BSONElement(0x04, bson[List[A]] {
      new Codec[List[A]] {

        def decode(bits: BitVector): Attempt[DecodeResult[List[A]]] = {

          //FIXME: think there is a SO here.
          def go(bits: BitVector, i : Int): Attempt[DecodeResult[List[A]]] = 
            if (bits.isEmpty)
              Attempt successful DecodeResult(Nil,bits)
            else
              element.decodeUnsafe(i.toString)(bits).flatMap{
                case DecodeResult(a,rem) => go(rem,i+1).map(_.map(a :: _))
              }

          go(bits,0)
        }

        def encode(as: List[A]): Attempt[BitVector] = 
          as.foldLeft(Attempt.successful(BitVector.empty) -> 0) { 
            case ((buff,i),a) =>
              val bits = element.encodeUnsafe(cstring encode i.toString)(a)
              (for { prev <- buff
                     next <- bits } yield prev ++ next, i + 1)
          }._1

        def sizeBound: SizeBound = SizeBound.unknown
      }
    })

/*
  /** Array (Dynamic).
    * Prefer this combinator when
    * <ul>
    *   <li/> Encoding, when the number and type of elements is unknown at compile-time.
    *   <li/> Decoding, when the number and type of elements is unknown at run-time.
    * </ul>
    * @tparam C `Coproduct` of `BSONElement[_]`.
    */
  def array[C <: Coproduct](e_list: C)(implicit wit: Witness.Aux[K]): BSONCodec[List[C]] =
    new BSONElement(0x04, bson[List[C]]{
      new Codec[List[C]] {
        def decode(bits: BitVector): Attempt[DecodeResult[List[C]]] = ???
        def encode(cs: List[C]): Attempt[BitVector] = ???
        def sizeBound: SizeBound = SizeBound.unknown
      }
    }).apply[K]
*/

  /** Binary data. The BSON "binary" or "BinData" datatype is used to
    * represent arrays of bytes. It is somewhat analogous to the Java notion
    * of a ByteArray. BSON binary values have a subtype. This is used to
    * indicate what kind of data is in the byte array. Subtypes from zero to
    * 127 are predefined or reserved. Subtypes from 128-255 are user-defined.
    */
  def binary: BSONElement[ByteVector] = new BSONElement(0x05, _binary)

  /** MongoDB ObjectId. 
    * @see [[ObjectId]]
    */
  def objectId: BSONElement[ObjectId] = new BSONElement(0x07, ObjectId.codec)

  /** Boolean. */
  def boolean: BSONElement[Boolean] = new BSONElement(0x08, ignore(7) ~> bool)

  /** UTC Datetime. The int64 is UTC milliseconds since the Unix epoch. */
  def utc: BSONElement[Instant] =
    new BSONElement(0x09, types.int64.xmap(Instant.ofEpochMilli,_.toEpochMilli))

  /** Explicitly indicates a missing value.
    * @see [[[option[K]:OptionSyntax[K]* option]]] combinator for a type-safe alternative.
    */
  def `null`: BSONElement[None.type] = new BSONElement(0x0A, provide(None))

  /** Option combinator.
    * This overload is purely syntax to allow specifying the label name before
    * the codec.
    * @return A `Codec` (not an `BSONElement`).
    * @example {{{
    * document['passport](utf8['firstName] ~ option['middleName](utf8) ~ utf8['surname])
    * }}}
    */
  def option[K]: OptionSyntax[K] = new OptionSyntax[K](this)

  /** Option combinator.
    * When decoding, either [[null]] or wrong field results in `None`.
    * @param encodeNulls If true, `None` will be encoded to a `Null` (`0x0A`) field. 
    *                   By default, `None` is not encoded.
    */
  def option[A](ename : BSONElement[A], encodeNulls: Boolean): BSONElement[Option[A]] = 
    new BSONElement[Option[A]](ename.opcode,ename.value.xmap(Option(_),_.get))
    with OptionalElement[A] { 
      val encodeNull = encodeNulls
    }
    

  /** Regular Expression.
    * @note `java.util.regex.Pattern.flags` are inlined because there is no
    * (public) scala constructor that allows passing these. */
  def regex: BSONElement[Regex] = 
    new BSONElement(0x0B, { (cstring ~ cstring).xmapc 
      { case (pat, spec) => 
        new Regex (if (spec.isEmpty) pat else "(?" + spec + ")" + pat)
      }
      { _.regex -> "" }
    })

  /* Investigate using Scala.js to compile this code
  def javascript[K,A,B](implicit e_name : String[K]) : Codec[FieldType[K,A => B]]

  def scopedJavascript[K](implicit e_name : String[K]) : Codec[FieldType[K,(String, A => B)]]
    0x0f ~> e_name(code_w_s)
  */

  /** 32-bit integer. */
  def int32: BSONElement[Int] = new BSONElement(0x10, types.int32)

  /** Timestamp. Special internal type used by MongoDB replication and
    * sharding. First 4 bytes are an increment, second 4 are a timestamp.
    * @note This is exposed as a `ULong` as there is nothing in the spec saying otherwise.
    * @see [[Timestamp]].
    */
  def uint64: BSONElement[types.ULong] = new BSONElement(0x11, types.uint64)

  /** 64-bit integer. */
  def int64: BSONElement[Long] = new BSONElement(0x12, types.int64)

  /** 128-bit decimal floating point. 
    * The [[scodec.protocols.bson.BasicTypes.Decimal128 Decimal128]] type is opaque and
    * implementation dependent. To create such types and manipulate them, you
    * will require some imports:
    * @example {{{
    * import scodec.Codec
    * import scodec.protocols.bson.types.Decimal128
    * import scodec.protocols.bson.syntax._
    * import scodec.protocols.bson.implicits._
    * 
    * case class Foo(huge: Decimal128)
    * 
    * Codec[Foo] encode Foo(Decimal128("1.2345E+999") + Decimal128("0.0001E+999")) require
    * // res1: BitVector(216 bits, 0x1b0000001368756765002300c0000000000000000000000049c600)
    * }}}
    * @see [[scodec.protocols.bson.BasicTypes.Decimal128]]
    * @see [[https://github.com/mongodb/specifications/blob/master/source/bson-decimal128/decimal128.rst]] 
    */
  def decimal128: BSONElement[types.Decimal128] = new BSONElement(0x13, types.decimal128)

  /** MongoDB Min key. Special type which compares lower than all other possible BSON element values. */
  def minKey: BSONElement[MinKey.type] = new BSONElement(Byte.MinValue, provide(MinKey))

  /** MongoDB Max key. Special type which compares higher than all other possible BSON element values. */
  def maxKey: BSONElement[MaxKey.type] = new BSONElement(0x7F, provide(MaxKey))

  /** String. The int32 is the number bytes in the (byte*) + 1 (for the 
    * trailing '\x00').  The (byte*) is zero or more UTF-8 encoded characters.
    *
    * FIXME : We use a trick of subtracting 1 from the size to avoid consuming the last byte. This will however incorrectly encode a utf string inlcuding 0x00 bytes - need to protect against this.
    */
  protected def string =
    variableSizeBytes(types.int32.xmap(_ - 1, _ + 1), codecs.utf8) <~ 0x00 

  /** Binary. The int32 is the number of bytes in the (byte*). */
  protected def _binary : Codec[ByteVector] = 
    variableSizePrefixedBytes(types.int32, /*subtype*/ 0x00, codecs.bytes).xmap(_._2, () -> _)

  /* TODO For the time being, just support 0x00
  protected def subtype = ( 0x00  // Generic binary subtype

    :+: 0x01          // Function

    :+: 0x02          // Binary (Old)

    :+: 0x03          // UUID (Old)

    :+: 0x04          // UUID

    :+: 0x05          // MD5

    :+: 0x80-0xFF          // User defined

  ).choice //TODO use index discriminator
  */

  /** Code w/ scope  */
  //protected def code_w_s = int32 ~ string ~ sc.lazily(document)
}

protected [bson] object NonTerminals {

  class ArraySyntax[K](self: NonTerminals) extends AnyRef {
    def apply[M <: HList, ML <: HList, L <: HList](e_list : M)(
        implicit 
        wit: Witness.Aux[K],
        comapped: Comapped.Aux[M,BSONElement,L],
        zipWithValue: Zip.Aux[M :: L :: HNil, ML],
        foldLeft: LeftFolder.Aux[ML,(Attempt[BitVector],Int),ZipEncode.type,(Attempt[BitVector],Int)],
        decoder: DecodeHArray.Aux[L,M]
    ): BSONCodec[L] = self.array[M,ML,L](e_list).apply[K]

    def apply[A](element: BSONElement[A])(implicit wit: Witness.Aux[K]): BSONCodec[List[A]] = 
      self.array[A](element).apply[K]
  }

  class DocumentSyntax[K](self: NonTerminals) extends AnyRef {
    def apply[A, C[a] <: Codec[a]](e_list: C[A])(implicit wit: Witness.Aux[K]): BSONCodec[A] =
      self.document[A,C](e_list).apply[K]
  }

  class OptionSyntax[K](self: NonTerminals) extends AnyRef {
    def apply[A](ename: BSONElement[A])(implicit wit: Witness.Aux[K]): BSONCodec[Option[A]] =
      self.option[A](ename,false).apply[K](wit)
  }

  import shapeless.{Nat,Poly2}
  import shapeless.ops.nat.ToInt

  object ZipEncode extends Poly2 {
    implicit def any[A] : 
      Case.Aux[(Attempt[BitVector],Int),(BSONElement[A],A),(Attempt[BitVector],Int)] = at {
        case ((rem,i),(ename,a)) => 
          val bits = ename.encodeUnsafe(cstring encode i.toString)(a)
          (for { prev <- rem
                 next <- bits } yield prev ++ next, i + 1)
      }
  }

  trait DecodeHArray[L <: HList] {

    /** `L` wrapped in `Ename` */
    type BSONElements <: HList

    /** Recursively use each `ename` to decode bits */
    def apply(enames: BSONElements, bits: Attempt[BitVector], index: Int = 0): Attempt[DecodeResult[L]]
  }

  object DecodeHArray {

    type Aux[L <: HList, M <: HList] = DecodeHArray[L]{ type BSONElements = M }

    def apply[L <: HList](implicit d: DecodeHArray[L]): Aux[L,d.BSONElements] = d

    implicit val hnil: Aux[HNil,HNil] = 
      new DecodeHArray[HNil] {

        type BSONElements = HNil

        def apply(hnil: HNil, maybeRem: Attempt[BitVector], last: Int): Attempt[DecodeResult[HNil]] = 
          maybeRem map (rem => DecodeResult(HNil,rem))
      }

    implicit def hlist[H, T <: HList](implicit tail: DecodeHArray[T]): Aux[H :: T, BSONElement[H] :: tail.BSONElements] =
      new DecodeHArray[H :: T] {

        type BSONElements = BSONElement[H] :: tail.BSONElements

        def apply(enames: BSONElement[H] :: tail.BSONElements, maybeRem: Attempt[BitVector], index: Int): Attempt[DecodeResult[H :: T]] = {
          val codec = //TODO: this allocates a new BSONElement - see if this can be avoided. I can, but requires duplicating decoding logic. Consider adding a `dynamic` method to BSONElement?
            enames.head[Int](new Witness { type T = Int; val value = index })
          maybeRem flatMap { rem =>
            codec decode rem flatMap {
              case DecodeResult(h,rem) => 
                tail(enames.tail, Attempt successful rem, index + 1) map (_.map(h :: _))
            }
          }
        }
      }
  }
}

/** MongoDB Min key. Special type which compares lower than all other possible BSON element values. */
case object MinKey

/** MongoDB Max key. Special type which compares higher than all other possible BSON element values. */
case object MaxKey
