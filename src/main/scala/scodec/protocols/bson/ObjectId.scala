package scodec.protocols.bson

import scodec.Codec

/** ObjectIds are small, likely unique, fast to generate, and ordered. 
  * ObjectId values consist of 12 bytes, where the first four bytes are a 
  * timestamp that reflect the ObjectId’s creation.
  * @see [[http://dochub.mongodb.org/core/objectids]]
  */
case class ObjectId(seconds : Long, mid : Int, pid : Int, counter : Int)

object ObjectId {
  import scodec.codecs._
  def codec : Codec[ObjectId] = 
    (uint32L :: uintL(3*8) :: uintL(2*8) :: uintL(3*8)).as[ObjectId]
}
