package scodec.protocols.bson

import scodec._
import scodec.bits._
import scodec.codecs._
import shapeless.{Witness,Nat}
import shapeless.ops.nat._

/** Mix-in to make an `BSONElement` optional. */
trait OptionalElement[A] extends BSONElement[Option[A]] { self : BSONElement[Option[A]] =>

  val encodeNull: Boolean

  abstract override protected[bson] 
  def encodeUnsafe(labelBits: Attempt[BitVector])(oa: Option[A]): Attempt[BitVector] = {
    oa.fold{
      if (encodeNull)
      `null`.encodeUnsafe(labelBits)(None)
      else 
        Attempt successful BitVector.empty 
    } { _ =>
      super.encodeUnsafe(labelBits)(oa)
    }
  }

  abstract override protected[bson]
  def decodeUnsafe(label: String)(bits: BitVector): Attempt[DecodeResult[Option[A]]] = {
    super
      .decodeUnsafe(label)(bits)
      .recoverWith {
        case _ : KnownDiscriminatorType[_]#UnknownDiscriminator => 
          `null`.value decode bits map { case DecodeResult(nul,rem) =>
            DecodeResult(nul: Option[A],rem)
          }
      }
      .recover{ case _ => DecodeResult(Option.empty[A],bits) }
  }

}
