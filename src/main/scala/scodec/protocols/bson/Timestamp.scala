package scodec.protocols.bson

import scala.language.implicitConversions

/** Typeclass providing access to the different components of a BSON timestamp.
  * @example {{{
  * scala> import scodec.protocols.bson.types.{ULong,Timestamp}
  * import scodec.protocols.bson.{ULong, Timestamp}
  *
  * scala> import scodec.protocols.bson.syntax._
  * import scodec.protocols.bson.syntax._
  *
  * scala> val secs = scala.compat.Platform.currentTime / 1000 toInt
  * secs: Int = 1532942553
  *
  * scala> val t = Timestamp.fromSeconds(secs, 0)
  * t: scodec.protocols.bson.types.ULong = 3065885106
  *
  * scala> val s = t.next
  * s: scodec.protocols.bson.types.ULong = 3065885107
  *
  * scala> s > t
  * res0: Boolean = true
  *
  * scala> (s.seconds, s.ordinal)
  * res1: (Int, Int) = (1532942553,1)
  *
  * }}}
  *
  * @see [[https://docs.mongodb.com/manual/reference/bson-types/#timestamps]]
  */
trait Timestamp[T] {
  import Integral.Implicits.infixIntegralOps

  implicit val T : Integral[T]

  /** Seconds since Unix Epoch. */
  def seconds(t : T) : Int = (t / T.fromInt(2 << 32)).toInt

  /** An incrementing ordinal for operations within given [[seconds]].*/
  def ordinal(t : T) : Int = (t % T.fromInt(2 << 32)).toInt

  /** Returns the timestamp with the [[ordinal]] incremented by one. */
  def next(t : T) : T = (t + T.one)

  /** Create a timestamp from a combination of seconds/ordinal.
    * @param seconds Number of seconds from UnixEpoch.
    * @param ordinal Atomic counter within a second to make the timestmap unique.
    */
  def fromSeconds(seconds : Int, ordinal : Int) : T = 
    T.fromInt(ordinal) + T.fromInt(seconds) * T.fromInt(2 << 32)
}

object Timestamp {

  trait Ops[T] {
    val instance : T

    val typeclass : Timestamp[T]

    def seconds : Int = typeclass.seconds(instance)

    def ordinal : Int = typeclass.ordinal(instance)

    def next : T = typeclass.next(instance)
  }

  trait Syntax {
    implicit def postfixTimestampOps[T](t : T)(implicit tc : Timestamp[T]) : Ops[T]  = new Ops[T] {
      val instance = t
      val typeclass = tc
    }
  }
}
