package scodec.protocols 

import scodec.{Codec,Attempt,DecodeResult,Err,SizeBound,codecs => sc}
import scodec.codecs.{~,int64L}
import scodec.bits.BitVector

/** This package provides codecs for the BSON protocol.
  *
  * == Primitive Elements ==
  *
  * A BSON element is encoded in three parts:
  * <ol>
  *   <li/> A byte encoding the type (the 'opcode').
  *   <li/> A null terminated ASCII label.
  *   <li/> The value, encoded as per [[http://bsonspec.org/spec.html the offical spec]] 
  * </ol>
  *
  * [[BSONElement]]s are partially applied codecs, which know the type and
  * value, but not label, of the element. They are tarnsformed into
  * codecs by specifying the label as a singleton type, in square
  * brackets. i.e. ename `int32` becomes a codec `int32['age]`.
  *
  * == Combinators == 
  * Some combinators in this package operate on [[BSONElement]]s, rather 
  * than `Codec`s. e.g.: 
  *
  * {{{ 
  * document['address]{
  *   array['street](string) ~ 
  *   int32['zipCode] ~
  *   option['phone](int32)
  * }
  * }}}
  *
  * Here `document` takes a `Codec` as an argument, however
  * `option` and `array` take `BSONElement`s instead.
  *
  * @example {{{ scala> import scodec.protocols.bson.implicits._
  * import scodec.protocols.bson.implicits._
  *
  * scala> import scodec.Codec import scodec.Codec
  *
  * scala> case class Foo(bar : Int, baz : Long) defined class Foo
  *
  * scala> Codec[Foo].encode(Foo(1,2L)) res0:
  * scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(336
  * bits,
  * 0x2500000020000000106261720001000000120000001262617a0002000000000000000000000000000000))
  *
  * }}}
  */
package object bson extends Bson {

  /** A `BSONCodec` is simply a codec with leading byte and c-string
    * discriminators, so it will result in an `UnknownDiscriminator`
    * decoding error rather than the more generic `Err` when field
    * labels don't match. 
    */
  type BSONCodec[A] = Codec[A] with sc.KnownDiscriminatorType[Byte ~ String]

  lazy val types : BasicTypes = new BasicTypes {
    import scala.math.Numeric.{BigIntIsIntegral,BigDecimalIsFractional}
    import scala.math.Ordering.BigDecimalOrdering
    import BasicTypes.IEEE754Algebra

    type ULong = spire.ULong

    implicit val ULong = spire.ULong.integralInstance

    implicit val Timestamp = new Timestamp[ULong] { val T = ULong }

    type Decimal128 = BigDecimal

    implicit object Decimal128 
      extends IEEE754Algebra[Decimal128]
      with BigDecimalIsFractional with BigDecimalOrdering {
        def apply(s: String) = BigDecimal(s)
      }

    def uint64 : Codec[ULong] = int64L.xmap(l => new spire.ULong(l),_.toLong)

    def decimal128 : Codec[Decimal128] = BigDecimalIEEESA754.decimal128
  }
}
