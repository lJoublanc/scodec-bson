package scodec.protocols.bson.spire

import scala.language.implicitConversions
import annotation.tailrec

/** 64-bit unsigned long. Copied from [[https://github.com/non/spire/blob/0ee38b7abc9a42fe92a63c654498305ec80be454/core/src/main/scala/spire/math/ULong.scala spire]].
  * This is a `AnyVal` based implementation, making it quite efficient,
  * but with all associated caveats, see for example [[https://failex.blogspot.com/2017/04/the-high-cost-of-anyval-subclasses.html]]
  *
  * @author Copyright (c) 2011-2012 Erik Osheim, Tom Switzer
  */
class ULong(val signed: Long) extends AnyVal {
  final def toByte: Byte = signed.toByte
  final def toChar: Char = signed.toChar
  final def toShort: Short = signed.toShort
  final def toInt: Int = signed.toInt
  final def toLong: Long = signed

  final def toFloat: Float = {
    if (signed < 0) (ULong.LimitAsDouble + signed.toDouble).toFloat
    else signed.toFloat
  }

  // FIXME: it would be nice to write some "real" floating-point code
  // to correctly find the nearest Double.
  final def toDouble: Double =
    toBigInt.toDouble

  final def toBigInt: BigInt =
    if (signed < 0) ULong.LimitAsBigInt + signed
    else BigInt(signed)

  // FIXME: it would be nice to avoid converting to BigInt here
  override final def toString: String =
    if (signed >= 0L) signed.toString
    else (ULong.LimitAsBigInt + signed).toString

  // TODO: replace these with `===` and `=!=`? Don't we get `==` and `!=` for free with value classes?
  final def == (that: ULong): Boolean = this.signed == that.signed
  final def != (that: ULong): Boolean = this.signed != that.signed

  final def === (that: ULong): Boolean = this.signed == that.signed
  final def =!= (that: ULong): Boolean = this.signed != that.signed

  final def <= (that: ULong): Boolean = if (this.signed >= 0L)
    this.signed <= that.signed || that.signed < 0L
  else
    that.signed >= this.signed && that.signed < 0L

  final def < (that: ULong): Boolean = if (this.signed >= 0L)
    this.signed < that.signed || that.signed < 0L
  else
    that.signed > this.signed && that.signed < 0L

  @inline final def >= (that: ULong): Boolean = that <= this
  @inline final def > (that: ULong): Boolean = that < this

  final def unary_- : ULong = ULong(-this.signed)

  final def + (that: ULong): ULong = ULong(this.signed + that.signed)
  final def - (that: ULong): ULong = ULong(this.signed - that.signed)
  final def * (that: ULong): ULong = ULong(this.signed * that.signed)

  final def / (that: ULong): ULong = {
    val n: Long = this.signed
    val d: Long = that.signed

    if (d == 0) {
      throw new java.lang.ArithmeticException("/ by zero")
    } else if (d < 0) {
      ULong(if (n >= 0 || n < d) 0 else 1)
    } else if (n >= 0) {
      ULong(n / d)
    } else {
      val half = n >>> 1
      if (half < d) {
        ULong(1L)
      } else {
        ULong(((half / d) << 1) + (((half % d) << 1) + (n & 1)) / d)
      }
    }
  }

  final def % (that: ULong): ULong = this - (this / that) * that

  final def /% (that: ULong): (ULong, ULong) = {
    val q = this / that
    (q, this - q * that)
  }

  final def unary_~ : ULong = ULong(~this.signed)

  final def << (shift: Int): ULong = ULong(signed << shift)
  final def >> (shift: Int): ULong = ULong(signed >>> shift)
  final def >>> (shift: Int): ULong = ULong(signed >>> shift)
  final def & (that: ULong): ULong = ULong(this.signed & that.signed)
  final def | (that: ULong): ULong = ULong(this.signed | that.signed)
  final def ^ (that: ULong): ULong = ULong(this.signed ^ that.signed)

  final def ** (that: ULong): ULong = ULong.pow(1L, this.signed, that.signed)

  final def gcd (that: ULong): ULong = ULong.gcd(this, that)
}

object ULong {
  def integralInstance: Integral[ULong] = new Integral[ULong] {
    // Members declared in scala.math.Integral
    def quot(x: ULong,y: ULong): ULong = x / y
    def rem(x: ULong,y: ULong): ULong = x % y

    // Members declared in scala.math.Numeric
    def fromInt(x: Int): ULong = ULong.fromInt(x)
    def minus(x: ULong,y: ULong): ULong = x - y
    def negate(x: ULong): ULong = -x

    def plus(x: ULong,y: ULong) = x + y
    def times(x: ULong,y: ULong) = x * y
    def toDouble(x: ULong): Double = x.toDouble
    def toFloat(x: ULong): Float = x.toFloat
    def toInt(x: ULong): Int = x.toInt
    def toLong(x: ULong): Long = x.toLong

    // Members declared in scala.math.Ordering
    def compare(x: ULong,y: ULong): Int = 
      if (x <= y) {
        if (x === y) 0 else -1
      } else 1
  }

  @inline final def apply(n: Long): ULong = new ULong(n)

  final def apply(s: String): ULong = fromBigInt(BigInt(s))

  final def fromInt(n: Int): ULong = new ULong(n & 0xffffffffL)
  final def fromLong(n: Long): ULong = new ULong(n)

  final def fromBigInt(n: BigInt): ULong =
    if (n < 0) throw new IllegalArgumentException(s"$n < 0")
    else new ULong(n.toLong)

  implicit def ulongToBigInt(n: ULong): BigInt = n.toBigInt

  @inline final val MinValue = ULong(0L)
  @inline final val MaxValue = ULong(-1L)

  @tailrec final private[spire] def pow(t:Long, b:Long, e:Long): ULong = {
    if (e == 0L) new ULong(t)
    else if ((e & 1L) == 1L) pow(t * b, b * b, e >>> 1L)
    else pow(t, b * b, e >>> 1L)
  }

  @tailrec final private[spire] def gcd(a:ULong, b:ULong): ULong = {
    if (b == new ULong(0L)) a else gcd(b, a % b)
  }

  private[spire] final val LimitAsDouble: Double =
    math.pow(2.0, 64)

  private[spire] final val LimitAsBigInt: BigInt =
    BigInt(1) << 64
}
