// can't be in the same package as the protocols.bson as it messes with implicit
// resolution.
package scodec.protocols.bson.test 

import scala.language.postfixOps

import org.scalatest.{FlatSpec,Matchers}
import scodec.{Codec,Decoder,Attempt,DecodeResult}
import scodec.bits._
import shapeless._

/** The test examples are form [[http://bsonspec.org/faq.html]] */
class BsonSpec extends FlatSpec with Matchers {

  case class Example1(hello : String)
  val example1Bits = 
    hex"16000000" ++                                                // total document size
    hex"02"       ++                                                // 0x02 = type String
    ByteVector('h','e','l','l','o') ++ hex"00" ++                   // field name
    hex"06000000" ++ ByteVector('w','o','r','l','d') ++ hex"00" ++  // field value
    hex"00" bits                                                    // 0x00 = type EOO ('end of object')  

  case class Example2[L <: HList](`BSON` : L)
  case class Example2Dynamic(`BSON` : List[String :+: Double :+: Int :+: CNil])
  val example2Bits =
    hex"31000000" ++
    hex"04" ++ ByteVector('B','S','O','N') ++ hex"00" ++
    hex"26000000" ++
    hex"02300008000000" ++ ByteVector('a','w','e','s','o','m','e') ++ hex"00" ++
    hex"0131003333333333331440" ++
    hex"103200c2070000" ++
    hex"00" ++
    hex"00" bits

  type Hello = Witness.`"hello"`.T

  "Explicit Codec" should "encode FAQ example 1" in {
    import scodec.protocols.bson._

    bson(utf8[Hello]) encode "world" shouldEqual Attempt.successful(example1Bits)
  }

  it should "decode FAQ example 1" in {
    import scodec.protocols.bson._

    (bson(utf8[Hello]) decode example1Bits)
      .shouldEqual(Attempt.successful(DecodeResult("world", BitVector.empty))
      )
  }

  type BSON = Witness.`'BSON`.T

  it should "encode FAQ example 2" in {
    import scodec.protocols.bson._

    bson(array[BSON](utf8 :: double :: int32 :: HNil)) encode ("awesome" :: 5.05 :: 1986 :: HNil) shouldEqual 
     Attempt.successful(example2Bits)
  }

  it should "decode FAQ example 2" in {
    import scodec.protocols.bson._

    bson(array[BSON](utf8 :: double :: int32 :: HNil)) decode example2Bits shouldEqual 
      Attempt.successful(DecodeResult("awesome" :: 5.05 :: 1986 :: HNil,BitVector.empty))
  }

  /*
  it should "encode FAQ example 2 (dynamic)" in {
    import scodec.protocols.bson._

    bson(array(utf8 :+: double :+: int32)) encode("BSON" -> ("awesome" :: 5.05 :: 1986 :: HNil).toCoproduct[List]) shouldEqual 
     Attempt.successful(example2Bits)
  }

  it should "decode FAQ example 2 (dynamic)" in {
    import scodec.protocols.bson._

    bson(array(double :+: utf8 :+: int32)) decode example2Bits shouldEqual
    Attempt.successful {
      DecodeResult("BSON" -> List(
        Inr(Inl("awesome")),
        Inl(5.05),
        Inr(Inr(Inl(1986)))
      ) , BitVector.empty)
    }
  }
  */

  "Transformed Codec" should "encode FAQ example 1" in {
    import scodec.protocols.bson._

    bson(utf8[Hello]).as[Example1] encode Example1("world") shouldEqual Attempt.successful(example1Bits)
  }

  it should "encode FAQ example 2" in {
    import scodec.protocols.bson._

    bson(array[BSON](utf8 :: double :: int32 :: HNil))
      .as[Example2[String :: Double :: Int :: HNil]]
      .encode(Example2("awesome" ::  5.05 ::  1986 :: HNil)) 
      .shouldEqual(Attempt successful example2Bits)
  }

  "Implicit Codec" should "encode FAQ example 1" in {
    import scodec.protocols.bson.implicits._

    Codec encode Example1(hello = "world") shouldEqual Attempt.successful(example1Bits)
  }

  it should "decode FAQ example 1" in {
    import scodec.protocols.bson.implicits._

    Codec[Example1] decode example1Bits shouldEqual Attempt.successful(DecodeResult(Example1("world"),BitVector.empty))
  }

  it should "encode FAQ example 2" in {
    import scodec.protocols.bson.implicits._

    (Codec encode Example2(`BSON` = "awesome" :: 5.05 :: 1986 :: HNil))
      .shouldEqual(Attempt.successful(example2Bits))
  }

  it should "decode FAQ example 2" in {
    import scodec.protocols.bson.implicits._

    (Codec[Example2[String :: Double :: Int :: HNil]] decode example2Bits)
      .shouldEqual(Attempt.successful(DecodeResult(Example2("awesome" :: 5.05 :: 1986 :: HNil),BitVector.empty)))
  }

  /*
  it should "encode FAQ example 2 (dynamic)" in {
    import scodec.protocols.bson.implicits._

    Codec encode Example2Dynamic(("awesome" :: 5.05 :: 1986 :: HNil).toCoproduct[List]) shouldEqual
      Attempt.successful(example2Bits)
  }

  it should "decode FAQ example 2 (dynamic)" in {
    import scodec.protocols.bson.implicits._

    (Codec[Example2Dynamic] decode example2Bits)
      .shouldEqual{
        Attempt.successful(DecodeResult(Example2Dynamic{
          ("awesome" :: 5.05 :: 1986 :: HNil).toCoproduct[List]
        },BitVector.empty))
      }
  }
  */

  it should "encode nested case classes" in {
    import scodec.protocols.bson.implicits._
    case class Child(i: Int, b : Boolean)
    case class Parent(i: Int, c: Child)
    Codec encode Parent(-1,Child(-1,true)) shouldEqual Attempt.successful{
      hex"1f000000106900ffffffff03630010000000106900ffffffff086200010000".bits
    }
  }

  "Option codec" should "roundtrip implicitly (heterogeneous)" in {
    import scodec.protocols.bson.implicits._

    case class Missing(one: Int, three: String)

    case class Present(one: Int, two: Option[Long], three: String)

    val presentBitsNone = Codec encode Present(1,None,"three")

    val presentBitsSome = Codec encode Present(1,Some(2L),"three")

    val missingBits = Codec encode Missing(1,"three")

    withClue("middle value missing") {
      Codec[Present] decodeValue missingBits.require shouldEqual Attempt.successful {
        Present(1,None,"three")
      }
    }

    withClue("middle value null") {
      Codec[Present] decodeValue presentBitsNone.require shouldEqual Attempt.successful {
        Present(1,None,"three")
      }
    }

    withClue("middle value present") {
      Codec[Present] decodeValue presentBitsSome.require shouldEqual Attempt.successful {
        Present(1,Some(2L),"three")
      }
    }
  }

  it should "roundtrip implicitly (homogeneous)" in {
    import scodec.protocols.bson.implicits._

    case class Missing(one: Int, three: Int)
    
    case class Present(one: Int, two: Option[Int], three: Int)

    val missingBits = Codec encode Missing(1,3)

    Codec[Present] decodeValue missingBits.require shouldEqual Attempt.successful {
      Present(1,None,3)
    }
  }

  it should "handle None in last element (bug #2)" in {
    import scodec.protocols.bson._
   
    type A = Witness.`'a`.T
    type B = Witness.`'b`.T

    val c = bson(option[A](int32) ~ option[B](int32))
    
    withClue("(None,None)") {
      c decodeValue (c encode (None,None) require) shouldEqual (Attempt successful (None -> None))
    }
    withClue("(Some,None)") {
      c decodeValue (c encode (Some(-1),None) require) shouldEqual (Attempt successful (Some(-1) -> None))
    }
    withClue("(None,Some)") {
      c decodeValue (c encode (None,Some(-1)) require) shouldEqual (Attempt successful (None -> Some(-1)))
    }
  }
}
