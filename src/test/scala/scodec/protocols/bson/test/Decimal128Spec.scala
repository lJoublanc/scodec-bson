package scodec.protocols.bson.test

import scodec._
import scodec.bits._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.prop.GeneratorDrivenPropertyChecks.PropertyCheckConfiguration
import org.scalatest.{FlatSpec,Matchers}
import org.scalacheck.{Gen,Arbitrary}
import scodec.protocols.bson.BigDecimalIEEESA754

class Decimal128Spec extends FlatSpec with GeneratorDrivenPropertyChecks with Matchers {

  import BigDecimalIEEESA754._

  "DPD" should "encode examples" in {
    (dpd encode   5).map(_.toBin) shouldEqual Attempt.successful(bin"        0101".toBin)
    (dpd encode   9).map(_.toBin) shouldEqual Attempt.successful(bin"        1001".toBin)
    (dpd encode  55).map(_.toBin) shouldEqual Attempt.successful(bin"    101 0101".toBin)
    (dpd encode  79).map(_.toBin) shouldEqual Attempt.successful(bin"    111 1001".toBin)
    (dpd encode  99).map(_.toBin) shouldEqual Attempt.successful(bin"    101 1111".toBin)
    (dpd encode 555).map(_.toBin) shouldEqual Attempt.successful(bin"101 101 0101".toBin)
    (dpd encode 999).map(_.toBin) shouldEqual Attempt.successful(bin"001 111 1111".toBin)
  }

  it should "equal BCD for 0-79" in {
    import scodec.codecs.vpbcd
    forAll(Gen.choose[Long](0,79)) { x : Long =>
      val bcdRes = vpbcd encode x
      val dpdRes = bcdRes map (_.length) flatMap { 
        l => dpd encode x map (_ padLeft l) 
      }
      dpdRes.map(_.toBin) shouldEqual bcdRes.map(_.toBin)
    }
  }

  it should "roundtrip Long" in {
    forAll (Gen.posNum[Long]) { x : Long =>
      val encoded = dpd encode x
      val decoded = encoded flatMap ( dpd decodeValue _ )
      withClue("encoded")(encoded should matchPattern { case Attempt.Successful(_) => } )
      withClue("decoded")(decoded shouldEqual Attempt.successful(x))
    } 
  }

  /** This is based on the Appendix in 
    * [[http://http://speleotrove.com/decimal/dbcalc.html]]
    */ 
  "Decimal32" should "have correct extrema etc." in {
    import decimal32._
    val p = totCoeffLenDigits
  
    ELimit shouldEqual 191
    p shouldEqual 7
    EMax shouldEqual 96
    EMin shouldEqual -95
    bias shouldEqual 101
    withClue("Appendix #2: E_max") {
      maxValue shouldEqual BigDecimal(s"9.999999E$EMax")
    }
    withClue("Appendix #6: E_tiny") {
      tinyValue shouldEqual BigDecimal(s"0.000001E$EMin")
      (EMin - ETiny) shouldEqual (p - 1)
    }
  }

  /** This from [[http://speleotrove.com/decimal/dbspec.html]], here coeff is
    * selected to be 750 and exp 2 (arbitrarily?)
    * Note that the constant is three decimals, so most of that code isn't tested.
    */
  "Decimal64" should "roundtrip example -7.50" in {
    withClue("encoded") {
      (decimal64 encode BigDecimal(unscaledVal = -750, scale = 2)) shouldEqual (Attempt successful hex"A2 30 00 00 00 00 03 D0".bits)
    }
    withClue("decoded") {
      (decimal64 decodeValue hex"A2 30 00 00 00 00 03 D0".bits) shouldEqual (Attempt successful BigDecimal(-7.5))
    }
  }

  "Decimal128" should "roundtrip zero & extrema" in {
    import decimal128._
    import Attempt._

    def doCheck(codec: Codec[BigDecimal], asValue: BigDecimal, asBits: BitVector, clue: String) = {
      val res = for { bits <- codec encode asValue
                      x <- codec.complete decodeValue bits
                } yield bits -> x
      res match {
        case Successful((bits,x)) => 
          withClue(s"encoding $clue"){ bits.toBin shouldEqual asBits.toBin }
          withClue(s"decoding $clue"){ x shouldEqual asValue }
        case Failure(err) => err should matchPattern { case OutsideRangeErr(_,_) => }
      }
    }
    doCheck(decimal128, BigDecimal(0), BitVector.fromBin("0" * 128).get, "zero")
    doCheck(decimal128, tinyValue, BitVector.fromBin("0" * 127 + "1").get, "tiny")
    doCheck(decimal128, minValue, BitVector.fromBin("0" + "00 000" + "0000 0010 0001" + ("000 000 0000"*10) + "000 000 0001").get, "min")
    doCheck(decimal128, maxValue, BitVector.fromBin("0" + "11 101" + "1111 1111 1111" + ("001 111 1111"*11)).get, "max")
  }

  // Without this we can get false positives
  implicit override val generatorDrivenConfig: PropertyCheckConfiguration = 
    PropertyCheckConfiguration(minSuccessful = 500)

  it should "roundtrip" in {
    forAll { x : BigDecimal =>
      val encoded = decimal128.complete encode x
      val decoded = encoded flatMap ( decimal128 decodeValue _ )
      withClue("encoded"){
        encoded should matchPattern { 
          case Attempt.Successful(_) => 
          case Attempt.Failure(OutsideRangeErr(_,_)) =>
        } 
      }
      withClue("decoded"){
        decoded should (
          equal (Attempt.successful(x)) or
          matchPattern { case Attempt.Failure(OutsideRangeErr(_,_)) => }
        )
      }
    }
  }
}
