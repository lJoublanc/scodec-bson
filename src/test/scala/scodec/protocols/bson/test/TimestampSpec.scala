package scodec.protocols.bson.test 

import scala.language.postfixOps

import org.scalatest.{FlatSpec,Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalacheck.{Arbitrary,Gen}
import scodec.{Attempt,DecodeResult}
import scodec.codecs
import scodec.bits.BitVector
import scodec.protocols.bson.spire

class TimestampSpec extends FlatSpec with GeneratorDrivenPropertyChecks with Matchers {

   import scodec.protocols.bson.types.{ULong,Timestamp,uint64}
   import scodec.protocols.bson.syntax._

   implicit val arbUlong: Arbitrary[ULong] = Arbitrary {
     val max = BigInt(2).pow(64)
     Arbitrary.arbBigInt
       .arbitrary
       .filter(x => (0 <= x) && (x <= max))
       .map(x => spire.ULong.fromBigInt(x).asInstanceOf[ULong])
   }


   "uint64" should "should encode same as long > 0" in {
      forAll{ l : ULong =>
        val opaque = l.asInstanceOf[spire.ULong]
        val encoded = uint64 encode l
        val decoded = encoded flatMap (bits => uint64 decodeValue bits)
        withClue("encoded"){encoded shouldEqual (codecs.int64L encode opaque.signed)}
        withClue("decoded"){
          val i = codecs.int64L decodeValue encoded.require map (l => spire.ULong(l).asInstanceOf[ULong])
          decoded shouldEqual i
        }
      }
    }

    it should "roundtrip" in {
      // Without this we can get false positives
      forAll{ l: ULong =>
        val encoded = uint64 encode l
        val decoded = encoded flatMap (uint64 decode _)
        withClue("encoded"){ encoded should matchPattern { case Attempt.Successful(_) => } }
        withClue("decoded"){ decoded shouldEqual Attempt.successful(DecodeResult(l,BitVector.empty)) }
      }
    }
          
   "Timestamp" should "roundtrip" in {
     val secs = scala.compat.Platform.currentTime / 1000L toInt

     val t = Timestamp.fromSeconds(secs,0)

     val s = t.next

     assert(s > t, "s should be greated than t")

     (s.seconds, s.ordinal) shouldEqual (secs,1)
   }

   /* This would fail if the precision is not enough; typically the number would wrap around */
   it should "have 64-bit precision" in {
     val t = ULong.fromInt(2 << 32) * ULong.fromInt(2 << 31) + ULong.fromInt(2)
     t shouldBe > (ULong.fromInt(2 << 31))
   }
}
